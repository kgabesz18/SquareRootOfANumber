package kg.squareroot;

public class SquareRootOfANumber {
	
public static int[] loadArrayWithNaturalNumbers(int end) {
		
		int[] numberArray = new int[end];
		
		for(int i  = 0; i < numberArray.length; i++) {
			numberArray[i] = i;
		}
		
		return numberArray;
		
	}
	
	public static int[] loadArrayWithSquareNumbers(int end) {
		
		int[] numberArray = loadArrayWithNaturalNumbers(10);
		
		for(int i = 0; i < numberArray.length; i++) {
			numberArray[i] = numberArray[i] * numberArray[i];
		}
		
		return numberArray;
		
	}
	
	public static int findMinimum(int[] array, int number) {
		
		int min = array[0];
		
		for(int i = 0; i < array.length; i++) {
			if(array[i] < number) {
				min = array[i];
			}
		}
		
		return min;
	}
	
	public static int findMaximum(int[] array, int number) {
		int max = array[array.length - 1];
		
		for(int i = array.length - 1; i > 0; i--) {
			if(array[i] > number) {
				max = array[i];
			}
		}
		
		return max;
	}
	
	public static double findNumberRange(int[] arraySquare, int[] arrayRoot, int number) {
		
		int index = 0;
		
		for(int i = 0; i < arraySquare.length; i++) {
			if(arraySquare[i] == number) {
				index = i;
			}
		}
		
		return arrayRoot[index];
		
	}
	
	public static double checkForOriginalNumber(double guessedNumber) {
		
		return guessedNumber * guessedNumber;
		
	}
	
	public static boolean checkGuessingError(double currentError, double errorBottom, double errorTop) {
		
		boolean insideErrorRange = false;
		
		if(currentError > errorBottom && currentError < errorTop) {
			insideErrorRange = true;
		}else {
			insideErrorRange = false;
		}
		
		return insideErrorRange;
		
	}
	
	public static double calculateSquareRoot(int number, int lastNumber) {
		
		int[] squareRoots = loadArrayWithNaturalNumbers(lastNumber);
		int[] squareNumbers = loadArrayWithSquareNumbers(lastNumber);
		
		System.out.println("Printing out square roots!");
		
		
		for(int i : squareRoots) {
			System.out.print(i + " ");
		}
		
		System.out.println();
		
		System.out.println("Printing out square numbers!");
		
		for(int i : squareNumbers) {
			System.out.print(i + " ");
		}
		
		System.out.println();
		
		int squareMin = findMinimum(squareNumbers, number);
		int squareMax = findMaximum(squareNumbers, number);
		
		double startNumberRange = findNumberRange(squareNumbers, squareRoots, squareMin);
		double endNumberRange = findNumberRange(squareNumbers, squareRoots, squareMax);
		
		//System.out.println(startNumberRange + " " + endNumberRange);
		
		double guessedSquareRootNumber = 0.0;
		double guessedSquareNumber = 0.0;
		double guessingErrorBottom = 0.0002;
		double guessingErrorTop = 0.0004;
		double currentError = 0.0;
		boolean insideErrorRange = false;
		
		System.out.println();
		
		while(insideErrorRange == false) {
		
			int numberOfCycle = 1;
		
			for(int i = 0; i < numberOfCycle; i++) {
				
				guessedSquareRootNumber = (startNumberRange + endNumberRange) / 2;
				//System.out.println(guessedSquareRootNumber + " " + startNumberRange + " " + endNumberRange);
				guessedSquareNumber = checkForOriginalNumber(guessedSquareRootNumber);
				//System.out.println();
				
				if(guessedSquareNumber > number) {
					endNumberRange = guessedSquareRootNumber;
				}else {
					startNumberRange = guessedSquareRootNumber;
				}
			}
		
		currentError = Math.abs((double)number - guessedSquareNumber);
		//System.out.println(guessedSquareNumber + " " + currentError);
		insideErrorRange = checkGuessingError(currentError, guessingErrorBottom, guessingErrorTop);
		
		}
		
		return guessedSquareRootNumber;
		
	}

}
